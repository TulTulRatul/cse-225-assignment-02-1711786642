#ifndef PERSON_H_INCLUDED
#define PERSON_H_INCLUDED
#include"SortedType.h"
#include<string>
using std::string;
class Person
{
private:
    string name;
    string fathersName;
    string mothersName;
    string presentAddress;
    string permanentAddress;
    string nationalidNumber;
    string height;
    string weight;
    string hairColor;
public:

    string get_name();
    string get_fathers_name();
    string get_mothers_name();
    string get_present_address();
    string get_permanent_address();
    string get_nationalid_number();
    string get_height();
    string get_weight();
    string get_hairColor();
    //mutators
    void set_name(string newName);
    void set_fathers_name(string newFathersName);
    void set_mothers_name(string newMothersName);
    void set_present_address(string newPresentAddress);
    void set_permanent_address(string newPermanentAddress);
    void set_nationalid_number(string newNationalidNumber);
    void set_height(string newHeight);
    void set_weight(string newWeight);
    void set_hairColor(string newHairColor);
};

#endif // PERSON_H_INCLUDED


