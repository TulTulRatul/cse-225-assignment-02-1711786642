#ifndef COMPANY_H_INCLUDED
#define COMPANY_H_INCLUDED
#include<string>
#define MAX_ITEMS 15
#include"Person.h"
#include"Customer.h"
#include"UnsortedType.h"

template<class ItemType>
class Company:public Customer
{
private:
    ItemType company;
public:
    void set_company(ItemType newcompany);
    ItemType get_company();
    UnsortedType<Customer>customer;

};


#endif // COMPANY_H_INCLUDED
