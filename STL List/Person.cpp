#include "Person.h"

string Person::get_name()
{
    return name;
}
string Person::get_fathers_name()
{
    return fathersName;
}
string Person::get_mothers_name()
{
    return mothersName;
}
string Person::get_present_address()
{
    return presentAddress;
}
string Person::get_permanent_address()
{
    return permanentAddress;
}
string Person::get_nationalid_number()
{
    return nationalidNumber;
}
string Person::get_height()
{
    return height;
}
string Person::get_weight()
{
    return weight;
}
string Person::get_hairColor()
{
    return hairColor;
}
void Person::set_name(string newName)
{
    name=newName;
}
void Person::set_fathers_name(string newFatherName)
{
    fatherName=newFathersName;
}
void Person::set_mothers_name(string newMotherName)
{
    motherName=newMothersName;
}
void Person::set_present_address(string newPresentAddress)
{
    presentAddress=newPresentAddress;
}
void Person::set_permanent_address(string newPermanentAddress)
{
    permanentAddress=newPermanentAddress;
}
void Person::set_nationalid_number(string newNidNumber)
{
    nationalidNumber=newNationalidNumber;
}

