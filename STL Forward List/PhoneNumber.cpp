#include "PhoneNumber.h"
#include <iostream>
template <class ItemType>
PhoneNumber<ItemType>::PhoneNumber(){
 length = 0;
 listData = nullptr;
 currentPos = nullptr;
}
template<class ItemType>
PhoneNumber<ItemType>::~PhoneNumber(){
 MakeEmpty();
}
template<class ItemType>
bool PhoneNumber<ItemType>::IsFull(){
 try{
 NodeType* newNode = new NodeType;
 delete newNode;
 }
 catch (std::bad_alloc exception){

 return true;
 }
 return false;
}
template<class ItemType>
int PhoneNumber<ItemType>::GetLength(){
 return length;
}
template<class ItemType>
bool PhoneNumber<ItemType>::IsEmpty(){
 return (length == 0 && listData == nullptr);
}

template<class ItemType>
bool PhoneNumber<ItemType>::PutPhoneNumber(ItemType item){
 if (IsFull())
 return false;
 NodeType* newNode = new NodeType;
 newNode->info = item;
 NodeType* curr = listData;
 NodeType* prev = nullptr;
 bool posFound = false;
 while (curr != nullptr && !posFound){
 if (curr->info < item){
 prev = curr;
 curr = curr->next;
 }
 else {
 posFound = true;
 newNode->next = curr;
 if (prev == nullptr){
 listData = newNode;
 }
 else {
 prev->next = newNode;
 }
 }
 }
 if (!posFound)
 {
 posFound = true;
 newNode->next = curr;
 if (prev == nullptr) {
 listData = newNode;
 }
 else {
 prev->next = newNode;
 }
 }
 length++;
 return posFound;
}


template<class ItemType>
ItemType PhoneNumber<ItemType>::GetNextItem(){
 currentPos = ((currentPos == nullptr) ? listData : currentPos->next);
 return currentPos->info;
}
template<class ItemType>
bool PhoneNumber<ItemType>::DeletePhoneNumber(ItemType item){
 NodeType* curr = listData;
 NodeType* prev = nullptr;
 bool found = false;
 while (curr != nullptr && !found){
 if (curr->info == item) {
 found = true;
 if (prev == nullptr) {

 listData = curr->next;
 }
 else {
 prev->next = curr->next;
 }
 delete curr;
 length--;
 }
 else {
 prev = curr;
 curr = curr->next;
 }
 }
 return found;
}
template<class ItemType>
void PhoneNumber<ItemType>::ResetList(){
 currentPos = nullptr;

 template<class ItemType>
ItemType PhoneNumber<ItemType>::GetPhoneNumber(ItemType item, bool & found){
 NodeType* curr = listData;
 found = false;
 while (curr != nullptr && !found){
 if (curr->info != item)
 curr = curr->next;
 else{

 found = true;
 return curr->info;
 }
 }
 return item;
}

}


