#ifndef CUSTOMER_H_INCLUDED
#define CUSTOMER_H_INCLUDED
#include<string>
#define MAX_ITEMS 15

using std::string;

#include "Person.h"
#include "SortedType.h"


class Customer:public Person

{
private:
    string customerCredentials;
public:
    string get_customerCredentials();
    void set_customerCredentials(string newCustomerCredentials);


};

#endif // CUSTOMER_H_INCLUDED

