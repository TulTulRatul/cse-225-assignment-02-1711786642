#ifndef PhoneNumber_H_INCLUDED
#define PhoneNumber_H_INCLUDED

template <class ItemType>

class PhoneNumber
{
 struct NodeType {
 ItemType info = ItemType();
 NodeType* next = nullptr;
 };
public:
 PhoneNumber();
 ~PhoneNumber();
 bool IsFull();
 int GetLength();
 void MakeEmpty();
 bool IsEmpty();
 ItemType GetPhoneNumber(ItemType item, bool& found);
 bool PutPhoneNumber(ItemType item);
 bool DeletePhoneNumber(ItemType item);
 //Iterator Operations
 void ResetList();
 bool HasNextItem();
 ItemType GetNextItem();

private:
 NodeType* listData = nullptr;
 int length = 0;
 NodeType* currentPos = nullptr;
};
#endif

